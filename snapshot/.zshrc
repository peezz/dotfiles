export PATH=~/bin:$PATH
# Path to oh-my-zsh installation.
export ZSH=/home/peez/.oh-my-zsh
# Terminal theme
ZSH_THEME="peez"
# Plugins 
plugins=(git zsh-autosuggestions zsh-syntax-highlighting)
# ctrl space to accept auto complete suggestion
bindkey '^ ' autosuggest-accept
# Init oh-my-zsh
source $ZSH/oh-my-zsh.sh
# Aliases
alias please='sudo $(fc -ln -1)'
alias :q=exit
# Coloured man pages
man() {
    LESS_TERMCAP_mb=$'\e'"[1;31m" \
    LESS_TERMCAP_md=$'\e'"[1;31m" \
    LESS_TERMCAP_me=$'\e'"[0m" \
    LESS_TERMCAP_se=$'\e'"[0m" \
    LESS_TERMCAP_so=$'\e'"[1;44;33m" \
    LESS_TERMCAP_ue=$'\e'"[0m" \
    LESS_TERMCAP_us=$'\e'"[1;32m" \
    command man "$@"
}
# Auto start tmux if connecting via ssh
if [[ -z "$TMUX" ]] && [ "$SSH_CONNECTION" != "" ]; then
    tmux attach-session -t ssh_tmux || tmux new-session -s ssh_tmux
fi
