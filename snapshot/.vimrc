" Vundle Start
set nocompatible
filetype off
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" color scheme
Plugin 'cocopon/pgmnt.vim'
Plugin 'cocopon/iceberg.vim'
" color scheme end
Plugin 'VundleVim/Vundle.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'kien/ctrlp.vim'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'scrooloose/nerdtree'
" Auto complete
Plugin 'valloric/youcompleteme'
call vundle#end()
filetype plugin indent on    " required
" see :h vundle for more details or wiki for FAQ
" Vundle end
" let g:airline_symbols_ascii = 1
let g:NERDTreeDirArrowExpandable = '+'
let g:NERDTreeDirArrowCollapsible = '-'
let g:ctrlp_show_hidden = 1
let g:airline_theme='iceberg'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'default'
syntax enable
colorscheme iceberg
set number
set relativenumber
set fillchars+=vert:\ " remove pipe
set path+=**
set ttymouse=xterm2
set mouse=a
set hidden
set history=500
set wildmenu
set ignorecase
set smartcase
set title
set scrolloff=4
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set showcmd
set ruler
filetype plugin on
filetype indent on
" Keybinds
let mapleader =","
nnoremap <Tab> :bn<CR>
nnoremap <S-Tab> :bp<CR>
nnoremap <C-T> :tabnew<CR>
nnoremap <Leader>; :NERDTreeToggle<CR>

" airline theme:
runtime macros/matchit.vim

" highlight Normal guifg=#e0e0e0 guibg=#212121 gui=NONE ctermfg=254 ctermbg=235 cterm=NONE
" highlight NonText guifg=#99968b guibg=#212121 gui=NONE ctermfg=246 ctermbg=235 cterm=NONE
