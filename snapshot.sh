#!/usr/bin/env bash
# since my dotfiles in this directory are symlinks to thier actual location,
# in order to store the contents of the files in a repository I take a 
# snapshot of the files by copying them into a the /snapshot directory

# enable hidden files
shopt -s dotglob

# do the thing
for filename in *; do
	case $filename in
		README.md)
			echo ignoring $filename
			;;
		.git)
			echo ignoring $filename
			;;
		snapshot)
			echo ignoring $filename
			;;
		snapshot.sh)
			echo ignoring $filename
			;;
		*)
			echo copying $filename
			cp -Lr $filename snapshot/
			;;
	esac
done
