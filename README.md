# Dotfiles

a place for me to manage my configuration files

### If you are not me, what you are looking for probably is in the `snapshot/` directory

root level files are all symlinks. This is so I can manage these files easily.

So I can just do
`vim dotfiles/i3/config`
instead of remembering where the file actaully lives.
This also allows me to easily access other my other configuration files (very handy when you want to overhaul the aesthetic).

## Files:

- `i3/config` - i3 configuration (window manager)
- `polybar/config` - my polybar configuration (panel)
- `polybar/pulseaudio-tail.sh` - script for pulseaudio polybar module
- `rofi/config.rasi` - configuration and theme for rofi (program launcher)
- `terminator/config` - configuration and theme for termintator (terminal emulator)
- `.vimrc` - configuration for vim (text editor).
- `.xinitrc` - startup script for Xorg (display server)
- `.zshrc` - configuration for zsh (shell)
- `peez.zsh-theme` - theme for zsh
- `planar-reddit.css` - theme for reddit
- `README.md` - this file
- `snapshot.sh` - script to generate snapshot of above files for viewing on this repository

## Screencaps

one day
